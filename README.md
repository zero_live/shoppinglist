# Shopping List

## System dependencies
You can run this proyect with any web server but I am using docker with httpd image for dont have different web server in every computer.

- docker

## How run the proyect

You have to run the following command and visit with your browser `localhost:8080/src`:

- `./scripts/run`

For stop the proyect run `./scripts/stop`

## How run the tests

- After run the proyect you have to visit `localhost:8080/SpecRunner.html` for check unit test.
- For run all the test, unit and end to end you have to run the following command: `./scripts/test-all`

### Production
You can test the application in production visiting:

- `https://zero_live.gitlab.io/shoppinglist/`

or by clicking [here](https://zero_live.gitlab.io/shoppinglist/)
