import { List } from '../../src/javascript/domain/List.js'

describe('List', () => {
  const firstItem = 'firstItem'
  const middleItem = 'middleItem'
  const lastItem = 'lastItem'
  let list

  beforeEach(() => {
    list = new List()
  })

  it('retrieves listed items as array', () => {

    const listedItems = list.toArray()

    expect(listedItems).toEqual([])
  })

  it('adds items', () => {

    list.add(firstItem)

    expect(list.toArray()).toEqual([firstItem])
  })

  it('adds items at the beggining of the list', () => {
    const firstPosition = 0
    list.add(lastItem)

    list.add(firstItem)

    expect(list.toArray()[firstPosition]).toEqual(firstItem)
  })

  it('moves listed items to top position', () => {
    list.add(lastItem)
    list.add(middleItem)
    list.add(firstItem)

    list.moveTop(middleItem)

    expect(list.toArray()).toEqual([middleItem, firstItem, lastItem])
  })

  it('does nothing when moves to top the first item', () => {
    list.add(lastItem)
    list.add(middleItem)
    list.add(firstItem)

    list.moveTop(firstItem)

    expect(list.toArray()).toEqual([firstItem, middleItem, lastItem])
  })

  it('moves listed items to bottom position', () => {
    list.add(lastItem)
    list.add(middleItem)
    list.add(firstItem)

    list.moveBottom(middleItem)

    expect(list.toArray()).toEqual([firstItem, lastItem, middleItem])
  })

  it('does nothing when moves tp bottom the last item', () => {
    list.add(lastItem)
    list.add(middleItem)
    list.add(firstItem)

    list.moveBottom(lastItem)

    expect(list.toArray()).toEqual([firstItem, middleItem, lastItem])
  })

  it('removes an item', () => {
    list.add(lastItem)
    list.add(middleItem)
    list.add(firstItem)

    list.remove(middleItem)

    expect(list.toArray()).toEqual([firstItem, lastItem])
  })

  it('can be initialized from an array', () => {
    const intialArray = [firstItem, middleItem, lastItem]

    list = List.fromArray(intialArray)

    expect(list.toArray()).toEqual([firstItem, middleItem, lastItem])
  })
})
