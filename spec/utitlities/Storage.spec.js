import { Storage } from '../../src/javascript/utilities/Storage.js'
import { TestStorage } from "../support/TestStorage.js"

describe('Storage', () => {
  beforeEach(() => {
    TestStorage.clean()
  })

  afterEach(() => {
    TestStorage.clean()
  })

  it('persists a value list for a key', () => {
    const key = 'someKey'
    const valueList = ['someValue']

    Storage.persist(key, valueList)

    expect(Storage.retrieve(key)).toEqual(valueList)
  })
})
