import { TestStorage } from "../support/TestStorage.js"
import { TestPage } from "../support/TestPage.js"

describe('Page', () => {
  let items = []

  beforeEach(() => {
    items = []
    TestStorage.clean()
  })

  afterEach(() => {
    TestStorage.clean()
  })

  it('renders base elements', () => {

    const page = new TestPage({ items })

    expect(page.toText()).toContain('Your list')
    expect(page.toText()).toContain('add-item')
    expect(page.isFocusedNewItem()).toEqual(true)
    expect(TestStorage.hasItems()).toEqual(false)
  })

  it('adds a item to the list', () => {
    const newItem = 'newItem'
    const page = new TestPage({ items })

    page.fillNewItem(newItem)
    page.add()

    expect(page.toText()).toContain(newItem)
    expect(TestStorage.hasItems()).toEqual(true)
  })

  it('moves an item to top', () => {
    items = ['firstItem', 'secondItem']
    const page = new TestPage({ items })

    page.moveTopSecondItem()

    expect(page.topItem()).toContain('secondItem')
    expect(page.bottomItem()).toContain('firstItem')
  })

  it('moves an item to bottom', () => {
    items = ['firstItem', 'secondItem']
    const page = new TestPage({ items })

    page.moveBottomFirstItem()

    expect(page.topItem()).toContain('secondItem')
    expect(page.bottomItem()).toContain('firstItem')
  })

  it('removes an item', () => {
    items = ['firstItem']
    const page = new TestPage({ items })

    page.removeFirstItem()

    expect(page.toText()).not.toContain('firstItem')
    expect(TestStorage.hasItems()).toEqual(false)
  })

  it('does not add empty items', () => {
    const page = new TestPage({ items })

    page.fillNewItem('')
    page.add()

    expect(page.hasItems()).toEqual(false)
    expect(TestStorage.hasItems()).toEqual(false)
  })
})
