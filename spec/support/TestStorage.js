
export class TestStorage {
  static clean() {
    localStorage.clear()
  }

  static hasItems() {
    return localStorage.getItem('list') !== '[]'
  }
}