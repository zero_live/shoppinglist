import { Page } from "../../src/javascript/presentation/Page.js"
import { List } from "../../src/javascript/domain/List.js"
import { SpyDocument } from "./SpyDocument.js"

export class TestPage {
  constructor({ items }) {
    const list = List.fromArray(items)
    this.spyDocument = new SpyDocument()

    new Page(this.spyDocument, list).render()
  }

  fillNewItem(value) {
    this.spyDocument.fill('new-item', value)
  }

  add() {
    this.spyDocument.click('add-item')
  }

  moveTopSecondItem() {
    this.spyDocument.click('move-top-1')
  }

  moveBottomFirstItem() {
    this.spyDocument.click('move-bottom-0')
  }

  removeFirstItem() {
    this.spyDocument.click('item-0')
  }

  toText() {
    return this.spyDocument.toText()
  }

  topItem() {
    return this._splitedByFirstBottomArrow()[0]
  }

  bottomItem() {
    return this._splitedByFirstBottomArrow()[1]
  }

  hasItems() {
    return this.spyDocument.toText().includes('item-0')
  }

  isFocusedNewItem() {
    return this.spyDocument.isFocused('new-item')
  }

  _splitedByFirstBottomArrow() {
    return this.toText().split('move-bottom-0')
  }
}