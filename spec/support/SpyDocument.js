
export class SpyDocument {
  constructor() {
    this.elements = []
  }

  getElementById(id) {
    let element = this._findById(id)

    if (!element) {
      element = new Element(id)

      this.elements.push(element)
    }

    return element
  }

  fill(id, value) {
    const element = this.getElementById(id)

    element.fill(value)
  }

  click(id) {
    const element = this.getElementById(id)

    element.click()
  }

  toText() {
    const htmlElements = this.elements.map(element => element.innerHTML)

    return htmlElements.join('')
  }

  isFocused(id) {
    const element = this.getElementById(id)

    return element.isFocused()
  }

  _findById(id) {
    return this.elements.find(element => element.is(id))
  }
}

class Element {
  constructor(id) {
    this.id = id
    this.innerHTML
    this.value
    this.listeners = {}

    this.focused = false
  }

  fill(value) {
    this.value = value
  }

  addEventListener(event, callback) {
    this.listeners[event] = callback
  }

  click() {
    this.listeners.click()
  }

  focus() {
    this.focused = true
  }

  isFocused() {
    return this.focused
  }

  is(id) {
    return this.id === id
  }
}
