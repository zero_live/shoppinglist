
export class Document {
  constructor(document) {
    this.document = document
  }

  _addClickEventListener(id, callback) {
    const element = this._getElementById(id)

    element.addEventListener('click', callback)
  }

  _getElementById(id) {
    return this.document.getElementById(id)
  }
}