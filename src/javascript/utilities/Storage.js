
export class Storage {
  static persist(key, value) {
    const string = JSON.stringify(value)

    localStorage.setItem(key, string)
  }

  static retrieve(key) {
    const string = localStorage.getItem(key)

    return JSON.parse(string)
  }
}