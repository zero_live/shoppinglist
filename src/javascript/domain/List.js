
export class List {
  static TOP_POSITION() { return -1 }
  static BOTTOM_POSITION() { return 1 }

  static fromArray(items) {
    const list = new List()
    const orderedItems = items.reverse()

    orderedItems.forEach(item => list.add(item))

    return list
  }

  constructor() {
    this.items = []
  }

  add(item) {
    this.items.unshift(item)
  }

  moveTop(item) {
    if (this._isFirst(item)) { return }

    this._assign(item, List.TOP_POSITION())
  }

  moveBottom(item) {
    this._assign(item, List.BOTTOM_POSITION())
  }

  remove(item) {
    const position = this._position(item)

    this._removeItem(position)
  }

  toArray() {
    return this.items
  }

  _position(item) {
    return this.items.indexOf(item)
  }

  _assign(item, direction) {
    const itemPosition = this._position(item)
    const destination = itemPosition + direction

    this._move(itemPosition, destination)
  }

  _move(position, destination) {
    const item = this.items[position]

    this._removeItem(position)

    this._addIn(destination, item)
  }

  _removeItem(position) {
    this.items.splice(position, 1)
  }

  _addIn(position, item) {
    this.items.splice(position, 0, item)
  }

  _isFirst(item) {
    const firstIndex = 0

    return this._position(item) === firstIndex
  }
}