import { Storage } from '../utilities/Storage.js'

export class ListStorage {
  static LIST_KEY() { return 'list' }
  static EMPTY_LIST() { return [] }

  static persist(list) {
    Storage.persist(this.LIST_KEY(), list)
  }

  static retrieve() {
    const list = Storage.retrieve(this.LIST_KEY())

    return list || this.EMPTY_LIST()
  }
}