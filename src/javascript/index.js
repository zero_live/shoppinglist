import { Page } from './presentation/Page.js'
import { ListStorage } from './domain/ListStorage.js'
import { List } from './domain/List.js'

const list = List.fromArray(ListStorage.retrieve())

new Page(document, list).render()