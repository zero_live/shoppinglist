
export class ListTemplate {
  static moveTopId(value) { return `move-top-${value}` }
  static moveBottomId(value) { return `move-bottom-${value}` }
  static itemId(value) { return `item-${value}` }

  constructor(list) {
    this.list = list
  }

  toHtml() {
    const items = this.list.toArray().map(this._itemTemplate.bind(this))

    return items.join('')
  }

  _itemTemplate(item, index) {
    return `
    <div class="item">
      <a class="arrow-up" id="${ListTemplate.moveTopId(index)}"></a>
      <div class="item-text" id="${ListTemplate.itemId(index)}">${item}</div>
      <a class="arrow-down" id="${ListTemplate.moveBottomId(index)}"></a>
    </div>
    `
  }
}