import { ListTemplate } from './ListTemplate.js'

export class PageTemplate {
  static NEW_ITEM_ID() { return 'new-item' }
  static ADD_ITEM_ID() { return 'add-item' }

  constructor(list) {
    this.listTemplate = new ListTemplate(list)
  }

  toHtml() {
    const list = this.listTemplate.toHtml()

    return `
      <div class="title">Your list</div>
      <input type="text" name="${PageTemplate.NEW_ITEM_ID()}" id="new-item"><a id="${PageTemplate.ADD_ITEM_ID()}" class="as-button">Add</a>
      <div id="items">
        ${list}
      </div>
    `
  }
}