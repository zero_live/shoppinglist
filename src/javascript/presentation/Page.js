import { PageTemplate } from './templates/PageTemplate.js'
import { ListTemplate } from './templates/ListTemplate.js'
import { ListStorage } from '../domain/ListStorage.js'
import { Document } from '../utilities/Document.js'

export class Page extends Document {
  static ID() { return 'page' }

  constructor(document, list) {
    super(document)

    this.list = list
    this.template = new PageTemplate(list)
  }

  render() {
    ListStorage.persist(this.list.toArray())

    this._drawTemplateInPage()

    this._addEventListeners()
    this._newItem().focus()
  }

  addItem() {
    if (this._isEmptyNewItem()) { return }

    this.list.add(this._newItemValues())

    this.render()
  }

  moveTop(item) {
    this.list.moveTop(item)

    this.render()
  }

  moveBottom(item) {
    this.list.moveBottom(item)

    this.render()
  }

  remove(item) {
    this.list.remove(item)

    this.render()
  }

  _drawTemplateInPage() {
    this.page().innerHTML = this.template.toHtml()
  }

  _newItem() {
    const element = this._getElementById(PageTemplate.NEW_ITEM_ID())

    return element
  }

  _newItemValues() {
    return this._newItem().value
  }

  _isEmptyNewItem() {
    return this._newItemValues() === ''
  }

  _addEventListeners() {
    this._addClickEventListener(PageTemplate.ADD_ITEM_ID(), this.addItem.bind(this))

    const items = this.list.toArray()
    items.forEach(this._addEventListenerMoveTop.bind(this))
    items.forEach(this._addEventListenerMoveBottom.bind(this))
    items.forEach(this._addEventListenerRemove.bind(this))
  }

  _addEventListenerMoveTop(item, index) {
    this._addClickEventListener(ListTemplate.moveTopId(index), this.moveTop.bind(this, item))
  }

  _addEventListenerMoveBottom(item, index) {
    this._addClickEventListener(ListTemplate.moveBottomId(index), this.moveBottom.bind(this, item))
  }

  _addEventListenerRemove(item, index) {
    this._addClickEventListener(ListTemplate.itemId(index), this.remove.bind(this, item))
  }

  page() {
    return this._getElementById(Page.ID())
  }
}
