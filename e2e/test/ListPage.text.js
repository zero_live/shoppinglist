
describe('ListPage', () => {
  it('happy path', () => {
    cy.visit('/src/')

    cy.get('#new-item').type('firstItem')
    cy.get('#add-item').click()
    cy.get('#new-item').type('secondItem')
    cy.get('#add-item').click()

    cy.get('#item-0').contains('secondItem')
    cy.get('#item-1').contains('firstItem')

    cy.get('#move-bottom-0').click()

    cy.get('#item-0').contains('firstItem')
    cy.get('#item-1').contains('secondItem')

    cy.get('#move-top-1').click()

    cy.get('#item-0').contains('secondItem')
    cy.get('#item-1').contains('firstItem')

    cy.get('#item-0').click()

    cy.get('#item-0').contains('firstItem')
  })
})